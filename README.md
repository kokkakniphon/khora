# Welcome to KHORA: Sound Controller Program

   This program allows the user to create a user-own custom sound controller. 
By constructing audio nodes and transitions between them, the user can  implement the specific logic of how to sound will be played.
The program will play the sound accordingly to the implemented condition.
Most importantly, the user can include the provided header files and play the sound controller inside other c++ projects.

![KHORA Preview](./Readme/Khora_Preview.png)

## Basic Controls
- **CTRL + A** : Create a new NODE at the mouse position.
- **CTRL + D** : Delete a selected NODE / LINK.

KhorA: is under Apache 2.0 license

### Credit
- Niphon Prasophol
- Kongtai Pokapalakorn
- Thanathorn Nilkhao