#include "Input.h"

using namespace KHORA;

Vector2Int Input::mousePosition = Vector2Int();
Uint8 Input::m_KeyCurrentState[SDL_NUM_SCANCODES];
Uint8 Input::m_KeyPreviousState[SDL_NUM_SCANCODES];
std::vector<Uint8> Input::m_MouseDatas = std::vector<Uint8>();
std::vector<Uint8> Input::m_MouseDownDatas = std::vector<Uint8>();
std::vector<Uint8> Input::m_MouseUpDatas = std::vector<Uint8>();