
#include <iostream>
#include "Core/Engine.h"

int main(int argc, char** argv)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	
	KHORA::Engine::GetInstance().Run();

	return 0;
}
