#pragma once

#include "imgui.h"
#include "imgui_stdlib.h"

#include <vector>
#include <string>

namespace KHORA
{
	class Inspector
	{
	private:
		static Inspector* s_Instance;

		std::vector<std::string> fileNames;

		bool isPlaying = false;

		std::vector<unsigned int> deleteLinkList;

		int GetCurrentComboIndexNode(std::string name);
		int GetCurrentComboIndexLink(std::string name);

	public:

		void FetchData();

		void ShowWindow();

		inline static Inspector& GetInstance()
		{
			if (s_Instance == nullptr)
			{
				s_Instance = new Inspector();
				s_Instance->FetchData();
			}
			return *s_Instance;
		}

		inline void Clean() { delete s_Instance; }

	};
}


