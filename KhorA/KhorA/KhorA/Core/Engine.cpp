#include "Engine.h"

#include "Input.h"

#include "imgui_internal.h"
#include "FileManager.h"

#include <iostream>
#include <windows.h>

namespace KHORA
{

	Engine* Engine::s_Instance = nullptr;
	SDL_Event Engine::s_Event;

	static ImGuiID s_DockspaceID = 0;
	static SDL_Rect scene_view;

	static std::string s_GuiPath = "../Assets/GUI/";

	// Return the keycode according to the given value.
		// I know this look ungly but it work!!
	char ScancodeToKeycode(SDL_Scancode scancode, bool shifted)
	{
		char value = SDL_GetKeyFromScancode(scancode);
		if (shifted == true) // Convert the lower case to upper cause according to QWERTY keyboard layout.
		{
			if (value == 34)
				value += 5;
			else if (value == 45)
				value += 50;
			else if (value == 48)
				value -= 7;
			else if (value == 49 || (value >= 51 && value <= 53))
				value -= 16;
			else if (value == 50)
				value += 14;
			else if (value == 55 || value == 57)
				value -= 17;
			else if (value == 54)
				value += 40;
			else if (value == 56)
				value -= 14;
			else if (value == 58)
				value += 1;
			else if (value == 60 || value == 62 || value == 63)
				value -= 16;
			else if (value == 61)
				value -= 18;
			else if (value == 96)
				value += 30;
			else if (value >= 97 && value <= 125)
				value -= 32;
		}
		return value;
	}

	void Engine::LoadGUI()
	{
		//Load all file in GUI folder.
		std::string pattern(s_GuiPath);
		pattern.append("\\*");
		WIN32_FIND_DATA data;
		HANDLE hFind;
		if ((hFind = FindFirstFile(pattern.c_str(), &data)) != INVALID_HANDLE_VALUE) {
			do {
				std::string tempPath = s_GuiPath + data.cFileName;

				SDL_Texture* texture = IMG_LoadTexture(m_Renderer, tempPath.c_str());
				if (texture)
				{
					m_Textures[data.cFileName] = texture;
					std::cout << "texture: [" << tempPath << "] loaded!" << std::endl;
				}
			} while (FindNextFile(hFind, &data) != 0);
			FindClose(hFind);
		}
	}

	void Engine::SetupDockSpace()
	{
		bool active = true;

		static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;
		ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;

		window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
		window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

		ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);

		ImGui::Begin("Master Window", &active, window_flags);

		// Declare Central dockspace
		s_DockspaceID = ImGui::GetID("HUB_DockSpace");
		ImGui::DockSpace(s_DockspaceID, ImVec2(0.0f, 0.0f), dockspace_flags);

		// Show main menu bar.
		MainMenuBar::GetInstance().ShowWindow();

		ImGui::End(); // Master Window

		ImGui::PopStyleVar();
	}

	

	void Engine::NewController()
	{
		AudioController* controller = new AudioController();
		controller->name = "New Controller";
		audioPlayer.SetAudioController(controller);
		FileManager::WriteYAMLData(controller);

		PlaygroundWindow::GetInstance().LoadData(audioPlayer.GetController());
		PlaygroundWindow::GetInstance().SaveData();

		Refresh();
	}

	void Engine::OpenController(std::string path)
	{
		audioPlayer.SetAudioControllerPath(path);
		PlaygroundWindow::GetInstance().LoadData(audioPlayer.GetController());
		PlaygroundWindow::GetInstance().SaveData();

		Refresh();
	}

	void Engine::Refresh()
	{
		MainMenuBar::GetInstance().FetchData();
		ParameterWindow::GetInstance().FetchData();
		Inspector::GetInstance().FetchData();
	}

	void Engine::Run()
	{
		Init();

		Input::Init();

		LoadGUI();

		while (m_IsRunning)
		{
			if (SDL_PollEvent(&s_Event) != 0)
				Input::Listen();
			Input::Update();
			Events();
			Update();
			Render();
			Input::Clear();
		}

		Clean();
	}


	bool Engine::Init()
	{
		if (SDL_Init(SDL_INIT_VIDEO) != 0 && IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG) != 0) // If fail initializing SDL return false
		{
			SDL_Log("Failed to initialize SDL: %s", SDL_GetError());
			return false;
		}

		auto wflags = (SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
		m_Window = SDL_CreateWindow("KHORA", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, wflags);

		if (m_Window == nullptr)
		{
			SDL_Log("Failed to create Window: %s", SDL_GetError());
			return false;
		}


		m_Renderer = SDL_CreateRenderer(m_Window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
		if (m_Renderer == nullptr)
		{
			SDL_Log("Failed to create Renderer: %s", SDL_GetError());
			return false;
		}

		m_ClearColor = DARK;

		SDL_Init(SDL_INIT_EVERYTHING);

		SDL_SetWindowResizable(m_Window, SDL_bool(false));

		ImGui::CreateContext();
		ImGuiSDL::Initialize(m_Renderer, SCREEN_WIDTH, SCREEN_HEIGHT);

		ImGuiIO& io = ImGui::GetIO();
		io.KeyRepeatDelay = 0.5f;
		io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
		io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;



		//Initializa an audio controller
		//std::shared_ptr<AudioController> controller = std::make_shared<AudioController>();

		//Create a default parameter
		//Parameter defaultParam("Default", Boolean);

		//Use a default parameter as a global parameter
		//controller->AddGlobalParameter(defaultParam);

		//create an entry node
		//controller->CreateAudioNode("Entry", 0.0f, true, "");

		//Initializa an audio player
		//AudioPlayer::GetInstance()->SetAudioController(controller);

		imnodes::Initialize();

		return m_IsRunning = true;
	}

	void Engine::ShowWelcomePopup()
	{
		if (ImGui::BeginPopup("WelcomePopup"))
		{
			ImGui::NewLine();
			ImGui::TextColored(ImVec4(1.0f, 1.0f, 1.0f, 1.0f), "			  Welcome to KHORA: Sound Controller Program");
			ImGui::NewLine();
			ImGui::NewLine();
			ImGui::SameLine(30, 0);
			ImGui::TextColored(ImVec4(0.75f, 0.75f, 0.75f, 1.0f), "		This program allows the user to create a user-own \n	custom sound controller. By constructing audio nodes and \n transitions between them, the user can implement the specific \n  logic of how the sound will be played. The program will play \n       the sound accordingly to the implemented condition. \nMost importantly, the user can include the provided header files \n     and play the sound controller inside other c++ projects.");
			ImGui::NewLine();
			ImGui::Separator();
			ImGui::TextColored(ImVec4(0.24f, 0.65f, 0.89f, 1.0f), "\n							Basic Controls						\n							--------------						");
			ImGui::TextColored(ImVec4(1.0f, 1.0f, 0.0f, 1.0f), "		CTRL + A	"); ImGui::SameLine(); ImGui::TextColored(ImVec4(1.0f, 1.0f, 1.0f, 1.0f), "Create a new NODE at the mouse position.");
			ImGui::TextColored(ImVec4(1.0f, 1.0f, 0.0f, 1.0f), "		CTRL + D	"); ImGui::SameLine(); ImGui::TextColored(ImVec4(1.0f, 1.0f, 1.0f, 1.0f), "Delete a selected NODE / LINK.");
			ImGui::NewLine();
			ImGui::Text("  To LINK between NODE, Just simply connect the "); ImGui::SameLine(); ImGui::TextColored(ImVec4(1.0f, 1.0f, 0.0f, 1.0f), "input");  ImGui::SameLine(); ImGui::Text(" and "); ImGui::SameLine(); ImGui::TextColored(ImVec4(1.0f, 1.0f, 0.0f, 1.0f), "output.");

			if (ImGui::IsMouseClicked(0))
			{
				welcomePopup = false;
				ImGui::CloseCurrentPopup();
			}

			ImGui::EndPopup();
		}
	}

	void Engine::Update()
	{
		ImGuiIO& io = ImGui::GetIO();

		int wheel = 0;

		if (s_Event.type == SDL_WINDOWEVENT)
		{
			if (s_Event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED)
			{
				io.DisplaySize.x = static_cast<float>(s_Event.window.data1);
				io.DisplaySize.y = static_cast<float>(s_Event.window.data2);
			}
		}
		else if (s_Event.type == SDL_MOUSEWHEEL)
		{
			wheel = s_Event.wheel.y;
		}


		int mouseX, mouseY;
		const int buttons = SDL_GetMouseState(&mouseX, &mouseY);

		// Setup low-level inputs (e.g. on Win32, GetKeyboardState(), or write to those fields from your Windows message loop handlers, etc.)

		io.MousePos = ImVec2(static_cast<float>(mouseX), static_cast<float>(mouseY));
		io.MouseDown[0] = buttons & SDL_BUTTON(SDL_BUTTON_LEFT);
		io.MouseDown[1] = buttons & SDL_BUTTON(SDL_BUTTON_RIGHT);
		io.MouseWheel = static_cast<float>(wheel);
		
		io.DeltaTime = 1.0f / 60.0f;

		io.KeyMap[ImGuiKey_Tab] = Input::GetKey(SDL_SCANCODE_TAB) ? SDL_SCANCODE_TAB : -1;
		io.KeyMap[ImGuiKey_LeftArrow] = Input::GetKey(SDL_SCANCODE_LEFT) ? SDL_SCANCODE_LEFT : -1;
		io.KeyMap[ImGuiKey_RightArrow] = Input::GetKey(SDL_SCANCODE_RIGHT) ? SDL_SCANCODE_RIGHT : -1;
		io.KeyMap[ImGuiKey_UpArrow] = Input::GetKey(SDL_SCANCODE_UP) ? SDL_SCANCODE_UP : -1;
		io.KeyMap[ImGuiKey_DownArrow] = Input::GetKey(SDL_SCANCODE_DOWN) ? SDL_SCANCODE_DOWN : -1;
		io.KeyMap[ImGuiKey_PageUp] = Input::GetKey(SDL_SCANCODE_PAGEUP) ? SDL_SCANCODE_PAGEUP : -1;
		io.KeyMap[ImGuiKey_PageDown] = Input::GetKey(SDL_SCANCODE_PAGEDOWN) ? SDL_SCANCODE_PAGEDOWN : -1;
		io.KeyMap[ImGuiKey_Home] = Input::GetKey(SDL_SCANCODE_HOME) ? SDL_SCANCODE_HOME : -1;
		io.KeyMap[ImGuiKey_End] = Input::GetKey(SDL_SCANCODE_END) ? SDL_SCANCODE_END : -1;
		io.KeyMap[ImGuiKey_Insert] = Input::GetKey(SDL_SCANCODE_INSERT) ? SDL_SCANCODE_INSERT : -1;
		io.KeyMap[ImGuiKey_Delete] = Input::GetKey(SDL_SCANCODE_DELETE) ? SDL_SCANCODE_DELETE : -1;
		io.KeyMap[ImGuiKey_Backspace] = Input::GetKey(SDL_SCANCODE_BACKSPACE) ? SDL_SCANCODE_BACKSPACE : -1;
		io.KeyMap[ImGuiKey_Space] = Input::GetKey(SDL_SCANCODE_SPACE) ? SDL_SCANCODE_SPACE : -1;
		io.KeyMap[ImGuiKey_Enter] = Input::GetKey(SDL_SCANCODE_RETURN) ? SDL_SCANCODE_RETURN : -1;
		io.KeyMap[ImGuiKey_Escape] = Input::GetKey(SDL_SCANCODE_ESCAPE) ? SDL_SCANCODE_ESCAPE : -1;
		io.KeyMap[ImGuiKey_KeyPadEnter] = Input::GetKey(SDL_SCANCODE_KP_ENTER) ? SDL_SCANCODE_KP_ENTER : -1;
		io.KeyMap[ImGuiKey_A] = Input::GetKey(SDL_SCANCODE_A) ? SDL_SCANCODE_A : -1;
		io.KeyMap[ImGuiKey_C] = Input::GetKey(SDL_SCANCODE_C) ? SDL_SCANCODE_C : -1;
		io.KeyMap[ImGuiKey_V] = Input::GetKey(SDL_SCANCODE_V) ? SDL_SCANCODE_V : -1;
		io.KeyMap[ImGuiKey_X] = Input::GetKey(SDL_SCANCODE_X) ? SDL_SCANCODE_X : -1;
		io.KeyMap[ImGuiKey_Y] = Input::GetKey(SDL_SCANCODE_Y) ? SDL_SCANCODE_Y : -1;
		io.KeyMap[ImGuiKey_Z] = Input::GetKey(SDL_SCANCODE_Z) ? SDL_SCANCODE_Z : -1;

		io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;

		ImGui::NewFrame();

		// Show all imgui.
		SetupDockSpace();

		//ImGui::ShowDemoWindow();

		if (m_SceneWindow)
			PlaygroundWindow::GetInstance().ShowWindow();
		Inspector::GetInstance().ShowWindow();
		ParameterWindow::GetInstance().ShowWindow();

		if (welcomePopup)
		{
			ImVec2 popupSize(500, 315);
			ImGui::SetNextWindowPos(ImVec2(SCREEN_WIDTH / 2 - popupSize.x / 2, SCREEN_HEIGHT / 2 - popupSize.y / 2));
			ImGui::SetNextWindowSize(popupSize);
			ImGui::OpenPopup("WelcomePopup");
		}

		ShowWelcomePopup();

		ImGui::EndFrame();

		audioPlayer.Update();
	}

	void KHORA::Engine::Render()
	{
		// Reset view port.
		SDL_SetRenderDrawColor(m_Renderer, m_ClearColor.r, m_ClearColor.g, m_ClearColor.b, m_ClearColor.a);
		SDL_RenderClear(m_Renderer);

		ImGui::Render();
		ImGuiSDL::Render(ImGui::GetDrawData());

		SDL_RenderPresent(m_Renderer);
	}

	void Engine::Events()
	{
		ImGuiIO& io = ImGui::GetIO();

		switch (s_Event.type)
		{
		case SDL_KEYDOWN:
		{
			for (size_t i = 0; i < 512; i++)
			{
				io.KeysDown[i] = false;
			}

			int key = s_Event.key.keysym.scancode;
			if (Input::GetKey(s_Event.key.keysym.scancode))
			{
				if (SDL_GetKeyFromScancode(s_Event.key.keysym.scancode) <= SDL_NUM_SCANCODES)
				{
					io.KeysDown[key] = true;
					io.AddInputCharacter(ScancodeToKeycode(s_Event.key.keysym.scancode, Input::GetKey(SDL_SCANCODE_LSHIFT) || Input::GetKey(SDL_SCANCODE_RSHIFT)));
				}
			}

			io.KeySuper = false;
			break;
		}
		case SDL_KEYUP:
		{
			io.KeyShift = ((SDL_GetModState() & KMOD_SHIFT) != 0);
			io.KeyCtrl = ((SDL_GetModState() & KMOD_CTRL) != 0);
			io.KeyAlt = ((SDL_GetModState() & KMOD_ALT) != 0);
			io.KeySuper = false;
			break;
		}
		case SDL_QUIT:
		{
			Quit();
			break;
		}
		}
	}

	void Engine::Quit()
	{
		m_IsRunning = false;
	}

	void Engine::Clean()
	{
		MainMenuBar::GetInstance().Clean();
		PlaygroundWindow::GetInstance().Clean();
		Inspector::GetInstance().Clean();
		ParameterWindow::GetInstance().Clean();

		imnodes::Shutdown();

		ImGuiSDL::Deinitialize();
		ImGui::DestroyContext();

		SDL_DestroyRenderer(m_Renderer);
		SDL_DestroyWindow(m_Window);
		SDL_Quit();

		//delete dynamically allocated memory
		delete s_Instance;
	}

}

