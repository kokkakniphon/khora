#pragma once

#include <vector>
#include <string>

namespace KHORA
{
	class MainMenuBar
	{
	private:
		static MainMenuBar* s_Instance;

		std::vector<std::string> fileNames;

		bool *unsaveChange = nullptr;

		std::string s_AssetControllerPath = "../Assets/Controller/";

		void ShowMenuFile();
		void FetchInput();
	
	public:

		void FetchData();

		void ShowWindow();

		inline static MainMenuBar& GetInstance()
		{
			if (s_Instance == nullptr)
			{
				s_Instance = new MainMenuBar();
				s_Instance->FetchData();
			}
			return *s_Instance;
		}

		inline void Clean()
		{
			delete s_Instance;
		}
	};
}
