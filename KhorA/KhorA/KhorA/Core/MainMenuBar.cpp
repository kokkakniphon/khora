#include "MainMenuBar.h"

#include <iostream>
#include <windows.h>
#include <direct.h>

#include "Input.h"
#include "Engine.h"

#include "PlaygroundWindow.h"

namespace KHORA
{
	MainMenuBar* MainMenuBar::s_Instance = nullptr;

	void MainMenuBar::FetchData()
	{
		//Load all file in GUI folder.
		std::string pattern(s_AssetControllerPath);
		pattern.append("\\*");
		WIN32_FIND_DATA data;
		HANDLE hFind;

		fileNames.clear();

		if ((hFind = FindFirstFile(pattern.c_str(), &data)) != INVALID_HANDLE_VALUE) {
			do {
				std::string fileName = data.cFileName;
				//if it's jpeg or png, load texture
				if (fileName == ".") continue;
				else if (fileName.substr(fileName.find_last_of(".") + 1) == "SoundController")
				{
					//remove file extension
					size_t lastindex = fileName.find_last_of(".");
					std::string rawname = fileName.substr(0, lastindex);
					//load texture and assign rawname as ID
					fileNames.push_back(rawname);
				}
			} while (FindNextFile(hFind, &data) != 0);
			FindClose(hFind);
		}
	}

	void MainMenuBar::ShowMenuFile()
	{
		if (ImGui::MenuItem("	New Controller		"))
		{
			Engine::GetInstance().NewController();
		}
		ImGui::Separator();
		if (ImGui::BeginMenu("	Open Controller		"))
		{
			for (size_t i = 0; i < fileNames.size(); i++)
			{
				if (ImGui::MenuItem(("	" + fileNames[i] + "	").c_str()))
				{
					Engine::GetInstance().OpenController(fileNames[i]);
				}
			}
			ImGui::EndMenu();
		}
		ImGui::Separator();
		if (ImGui::MenuItem("	Save		Ctrl+ S"))
		{
			Engine::GetInstance().SaveChanges();
		}
		ImGui::Separator();
		if (ImGui::MenuItem("	Refresh		F5"))
		{
			Engine::GetInstance().Refresh();
		}
		ImGui::Separator();
		if (ImGui::MenuItem("	Exit		"))
		{
			Engine::GetInstance().Quit();
		}
	}

	void MainMenuBar::FetchInput()
	{
		// Save Data
		if (Input::GetKey(SDL_SCANCODE_LCTRL) && Input::GetKeyDown(SDL_SCANCODE_S))
		{
			Engine::GetInstance().SaveChanges();
		}

		// Refresh
		if (Input::GetKeyDown(SDL_SCANCODE_F5))
		{
			Engine::GetInstance().Refresh();
		}
	}

	void MainMenuBar::ShowWindow()
	{
		if (unsaveChange == nullptr)
			unsaveChange = Engine::GetInstance().GetUnsaveChange();

		if (ImGui::BeginMenuBar())
		{
			FetchInput();

			if (ImGui::BeginMenu(((*unsaveChange) ? "*File" : "File")))
			{
				ShowMenuFile();
				ImGui::EndMenu(); // File
			}

			ImGui::EndMenuBar();
		}
	}

}