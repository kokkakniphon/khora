#include "Inspector.h"

#include <iostream>
#include <windows.h>
#include <direct.h>

#include "SDL_image.h"
#include "Engine.h"

#include "ParameterWindow.h"
#include "PlaygroundWindow.h"

namespace KHORA
{
	Inspector* Inspector::s_Instance = nullptr;

	int Inspector::GetCurrentComboIndexNode(std::string name)
	{
		for (size_t i = 0; i < fileNames.size(); i++)
		{
			if (fileNames[i] == name)
			{
				return i;
			}
		}
		return 0;
	}

	int Inspector::GetCurrentComboIndexLink(std::string name)
	{
		std::vector<Parameter>& globalParameter = ParameterWindow::GetInstance().GetGlobalParameters();

		for (size_t i = 0; i < globalParameter.size(); i++)
		{
			if (globalParameter[i].name == name)
			{
				return i;
			}
		}
		return 0;
	}

	void Inspector::FetchData()
	{
		//Load all file in GUI folder.
		std::string pattern(ASSET_AUDIO_PATH);
		pattern.append("\\*");
		WIN32_FIND_DATA data;
		HANDLE hFind;

		fileNames.clear();

		if ((hFind = FindFirstFile(pattern.c_str(), &data)) != INVALID_HANDLE_VALUE) {
			do {
				std::string fileName = data.cFileName;
				//if it's jpeg or png, load texture
				if (fileName == ".") continue;
				else if (fileName.substr(fileName.find_last_of(".") + 1) == "wav"||
						fileName.substr(fileName.find_last_of(".") + 1) == "ogg" ||
					fileName.substr(fileName.find_last_of(".") + 1) == "mp3" ||
					fileName.substr(fileName.find_last_of(".") + 1) == "flac" ||
					fileName.substr(fileName.find_last_of(".") + 1) == "mod" ||
					fileName.substr(fileName.find_last_of(".") + 1) == "it" ||
					fileName.substr(fileName.find_last_of(".") + 1) == "s3d" ||
					fileName.substr(fileName.find_last_of(".") + 1) == "xm")
				{
					//remove file extension
					size_t lastindex = fileName.find_last_of(".");
					std::string rawname = fileName.substr(0, lastindex);
					//load texture and assign rawname as ID
					fileNames.push_back(fileName);
				}
			} while (FindNextFile(hFind, &data) != 0);
			FindClose(hFind);
		}
	}

	void Inspector::ShowWindow()
	{

		if (ImGui::Begin("Inspector"))
		{
			if (ImGui::IsWindowFocused(ImGuiFocusedFlags_RootAndChildWindows) == false)
			{
				if (isPlaying)
				{
					Engine::GetInstance().GetAudioPlayer().Stop();
					isPlaying = false;
				}
			}

			Node* currentNode = PlaygroundWindow::GetInstance().GetCurrentSelectingNode();
			Link* currentLink = PlaygroundWindow::GetInstance().GetCurrentSelectingLink();

			if (currentNode != nullptr && currentNode->value->name != "Entry")
			{
				ImGui::Columns(2, NULL, false);

				ImGui::Text("		  Name "); ImGui::Spacing();
				ImGui::Text("		Volume "); ImGui::Spacing();
				ImGui::Text("		  Loop "); ImGui::Spacing();
				ImGui::Text("		Source "); ImGui::Spacing();

				ImGui::NextColumn();
				ImGui::PushItemWidth(-1);

				//Object's name
				if (ImGui::InputTextWithHint("##nodeName", "Node's name", &currentNode->value->name) 
					|| ImGui::SliderFloat("##nodeVolume", &currentNode->value->volume, 0.0f, 1.0f)
					|| ImGui::Checkbox("##nodeLoop ", &currentNode->value->isLoopable))
				{
					Engine::GetInstance().TriggerUnsaveChange();
				}

				int item_current_idx = GetCurrentComboIndexNode(currentNode->value->filePath);
				const char* combo_label = fileNames[item_current_idx].c_str();
				if (ImGui::BeginCombo("##nodeSource", combo_label))
				{
					for (int n = 0; n < fileNames.size(); n++)
					{
						const bool is_selected = (item_current_idx == n);
						if (ImGui::Selectable(fileNames[n].c_str(), is_selected))
						{
							currentNode->value->filePath = fileNames[n];
							item_current_idx = n;

							Engine::GetInstance().TriggerUnsaveChange();
						}

						if (is_selected)
							ImGui::SetItemDefaultFocus();
					}
					ImGui::EndCombo();
				}

				ImGui::PopItemWidth();
				ImGui::Columns(1);

				if (ImGui::CollapsingHeader("Preview", ImGuiTreeNodeFlags_DefaultOpen))
				{
					std::string currentImg = (isPlaying) ? "0203_square.png" : "0169_play.png";
					if (ImGui::ImageButton(Engine::GetInstance().GetTexture(currentImg), ImVec2(20.0f, 20.0f), ImVec2(0, 0), ImVec2(1, 1), -1, ImVec4(0, 0, 0, 0), ImVec4(1, 1, 1, 1)))
					{
						if (isPlaying == false)
						{
							Engine::GetInstance().GetAudioPlayer().Play(currentNode->value);
							isPlaying = true;
						}
						else
						{
							Engine::GetInstance().GetAudioPlayer().Stop();
							isPlaying = false;
						}

					}

					ImGui::SameLine();
					int currentProgression = Engine::GetInstance().GetAudioPlayer().GetCurrentProgression();
					ImGui::SetNextItemWidth(ImGui::GetWindowWidth() - 40.0f);
					if (ImGui::SliderInt("##nodePreviewPos", &currentProgression, 0, Engine::GetInstance().GetAudioPlayer().GetDuration()) && isPlaying == true)
					{
						Engine::GetInstance().GetAudioPlayer().SetCurrentProgression(currentProgression);
					}
					ImGui::Separator();
				}
			}
			else if (currentLink != nullptr)
			{
				if (ImGui::CollapsingHeader("Condition", ImGuiTreeNodeFlags_DefaultOpen))
				{
					std::vector<Parameter>& globalParameter = ParameterWindow::GetInstance().GetGlobalParameters();

					if (globalParameter.size() == 0)
					{
						ImGui::End();
						return;
					}

					for (unsigned int i = 0; i < currentLink->value->size(); i++)
					{
						int item_current_idx = GetCurrentComboIndexLink((*currentLink->value)[i].name);
						const char* combo_label = globalParameter[item_current_idx].name.c_str();
						ImGui::PushID(i);
						ImGui::SetNextItemWidth(ImGui::GetWindowWidth() * 0.5f);
						if (ImGui::BeginCombo("##linkName", combo_label))
						{
							for (int n = 0; n < globalParameter.size(); n++)
							{
								const bool is_selected = (item_current_idx == n);
								if (ImGui::Selectable(globalParameter[n].name.c_str(), is_selected))
								{
									(*currentLink->value)[i] = globalParameter[n];
									item_current_idx = n;

									Engine::GetInstance().TriggerUnsaveChange();
								}

								if (is_selected)
									ImGui::SetItemDefaultFocus();
							}
							ImGui::EndCombo();
						}

						if ((*currentLink->value)[i].type == ParameterType::Boolean)
						{
							ImGui::SameLine();
							if (ImGui::Checkbox("##linkBool", &(*currentLink->value)[i].checkValueBool))
							{
								Engine::GetInstance().TriggerUnsaveChange();
							}
						}

						if ((*currentLink->value)[i].type == ParameterType::Integer)
						{
							ImGui::SameLine();
							ImGui::SetNextItemWidth(ImGui::GetWindowWidth() * 0.25f);
							int item_current = (*currentLink->value)[i].isGreaterThan; // If it's true it's 1 if it's false it's 0
							if (ImGui::Combo("##linkIntGreater", &item_current, "Lower\0Greater\0\0"))
							{
								(*currentLink->value)[i].isGreaterThan = item_current;
								Engine::GetInstance().TriggerUnsaveChange();
							}
							ImGui::SameLine();
							if (ImGui::InputInt("##linkInt", &(*currentLink->value)[i].checkValueInt))
							{
								Engine::GetInstance().TriggerUnsaveChange();
							}
						}

						if ((*currentLink->value)[i].type == ParameterType::Float)
						{
							ImGui::SameLine();
							ImGui::SetNextItemWidth(ImGui::GetWindowWidth() * 0.25f);
							int item_current = (*currentLink->value)[i].isGreaterThan; // If it's true it's 1 if it's false it's 0
							if (ImGui::Combo("##linkIntGreater", &item_current, "Lower\0Greater\0\0"))
							{
								(*currentLink->value)[i].isGreaterThan = item_current;
								Engine::GetInstance().TriggerUnsaveChange();
							}
							ImGui::SameLine();
							if (ImGui::InputFloat("##linkInt", &(*currentLink->value)[i].checkValueFloat))
							{
								Engine::GetInstance().TriggerUnsaveChange();
							}
						}

						ImGui::PopID();
					}

					ImGui::Separator();

					ImGui::Spacing();
					ImGui::SameLine(0.0f, ImGui::GetWindowWidth() - 80.0f);

					if (ImGui::ImageButton(Engine::GetInstance().GetTexture("0170_plus-circle.png"), ImVec2(20.0f, 20.0f), ImVec2(0, 0), ImVec2(1, 1), -1, ImVec4(0, 0, 0, 0), ImVec4(1, 1, 1, 1)))
					{
						currentLink->value->push_back(globalParameter[0]);
						Engine::GetInstance().TriggerUnsaveChange();
					}

					ImGui::SameLine();

					if (ImGui::ImageButton(Engine::GetInstance().GetTexture("0143_minus-circle.png"), ImVec2(20.0f, 20.0f), ImVec2(0, 0), ImVec2(1, 1), -1, ImVec4(0, 0, 0, 0), ImVec4(1, 1, 1, 1)))
					{
						if (currentLink->value->size() > 0)
						{
							currentLink->value->pop_back();
							Engine::GetInstance().TriggerUnsaveChange();
						}
					}
				}
			}
		}
		ImGui::End();
	}

}
