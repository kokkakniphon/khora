#pragma once

#include "imgui.h"
#include "imgui_stdlib.h"

#include "AudioController.h"

namespace KHORA
{
	class ParameterWindow
	{
	private:
		static ParameterWindow* s_Instance;

		std::vector<Parameter>* globalParameter;

	public:

		void ShowWindow();

		std::vector<Parameter>& GetGlobalParameters() 
		{
			return *globalParameter;
		}

		inline static ParameterWindow& GetInstance()
		{
			if (s_Instance == nullptr)
			{
				s_Instance = new ParameterWindow();
			}
			return *s_Instance;
		}

		void FetchData();

		inline void Clean()
		{
			delete s_Instance;
		}
	};
}
