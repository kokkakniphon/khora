// Use this as an example for SoundPlayer
// This structure is a very good starting point,
// to understand the whole structure of the sound controller.

#pragma once

#include "irrKlang.h"
#include "AudioController.h"
#include "FileManager.h"

#define ASSET_AUDIO_PATH "../Assets/Audio/"
#define ASSET_CONTROLLER_PATH "../Assets/Controller/"

/*
#include "Object.h"

#include "AnimationController.h"
#include "SpriteRenderer.h"
#include "Time.h"

namespace DucklingsEngine
{
	class Animator : public Component
	{
	private:
		//
		// Summary:
		//      This is the target output render.
		SpriteRenderer *renderer = nullptr;

		std::string currentAnimationStateName = "";
		Animation *currentAnimation = nullptr;

		bool play = true;

	public:
		//
		// Summary:
		//     The Animation Controller to animate.
		std::shared_ptr<AnimationController> controller;

		Animator() = default;
		
		Animator(AnimationController* controller)
		{
			this->controller.reset(controller);
		}

		~Animator() = default;

		void SetAnimationController(std::shared_ptr<AnimationController> controller)
		{
			this->controller = controller;
			Start();
		}

		//
		// Summary:
		//     Returns the value of the given boolean parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		// Returns:
		//     The value of the parameter.
		inline bool GetBool(std::string name)
		{
			return controller->GetBool(name);
		}

		//
		// Summary:
		//     Returns the value of the given integer parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		// Returns:
		//     The value of the parameter.
		inline int GetInteger(std::string name)
		{
			return controller->GetInteger(name);
		}

		//
		// Summary:
		//     Sets the value of the given boolean parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		//   value:
		//     The new parameter value.
		inline void SetBool(std::string name, bool value)
		{
			return controller->SetBool(name, value);
		}

		//
		// Summary:
		//     Sets the value of the given integer parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		//   value:
		//     The new parameter value.
		inline void SetInteger(std::string name, int value)
		{
			return controller->SetInteger(name, value);
		}

		inline void SetPlay(bool play)
		{
			this->play = play;
		}

		int GetComponentClassID() override { return 95; }
		std::string GetComponentClassData() override 
		{ 
			std::string data = "";
			data += "Animator:\n";
			data += "  m_GameObject: {fileID: ";
			data += std::to_string(uniqueID);
			data += "}\n";
			data += "  m_Enable: ";
			data += this->enabled;
			data += "\n";
			return data;
		}

		bool Init() override 
		{
			return true;
		}
		void Start() 
		{
			if (controller == nullptr)
				return;

			renderer = &gameObject->GetComponent<SpriteRenderer>();

			currentAnimationStateName = controller->animations.begin()->first;
			currentAnimation = controller->animations[currentAnimationStateName].get();
			currentAnimation->animationStartTime = Time::GetTime();
			renderer->sprite = (controller->GetAnimationSprite(currentAnimationStateName));
			renderer->sprite->SetCurrentSpriteCell(0);

			if (renderer == nullptr)
				std::cout << "ERROR <Animator>: Can't find renderer attached to the gameObject." << std::endl;
		}
		void Draw() override
		{
			if (controller == nullptr || play == false)
				return;

			if (renderer != nullptr)
			{
				std::string desAnimation = controller->GetDestinationAnimation(currentAnimationStateName);
				if (currentAnimationStateName != desAnimation && desAnimation != "")
				{
					currentAnimationStateName = desAnimation;
					currentAnimation = controller->animations[currentAnimationStateName].get();
					currentAnimation->animationStartTime = Time::GetTime();
					renderer->sprite = controller->GetAnimationSprite(currentAnimationStateName);
					renderer->sprite->SetCurrentSpriteCell(0);
				}
				else
				{
					int targetFrame = (int)((Time::GetTime() - currentAnimation->animationStartTime) * currentAnimation->framePerSecond);
					renderer->sprite->SetCurrentSpriteCell(targetFrame);
				}
			}
		}

		void Update() {}

		void FixedUpdate() {}
	};
}
*/

namespace KHORA 
{
	class AudioPlayer 
	{
	private:
		std::string currentAudioStateName = "";
		AudioSource* currentAudio = nullptr;

		std::string controllerName = "";

		irrklang::ISound* sound = nullptr;

		bool play = false;
		bool pause = false;

		irrklang::ISoundEngine* s_AudioEngineInstance;

		//
		// Summary:
		//     The Audio Controller to play.
		std::shared_ptr<AudioController> controller = nullptr;

	public:


		AudioPlayer();

		//
		// Summary:
		//     Initialize audio player with the audio controller path.
		AudioPlayer(std::string name);

		~AudioPlayer() = default;

		inline std::string GetCurrentAudioState() {
			return currentAudioStateName;
		}

		inline AudioController* GetController() { return controller.get(); }

		void SetupController();

		void SetAudioControllerPath(std::string name);

		void SetAudioController(AudioController* controller);

		//
		// Summary:
		//     Returns the value of the given boolean parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		// Returns:
		//     The value of the parameter.
		inline bool GetBool(std::string name)
		{
			return controller->GetBool(name);
		}

		//
		// Summary:
		//     Returns the value of the given integer parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		// Returns:
		//     The value of the parameter.
		inline int GetInteger(std::string name)
		{
			return controller->GetInteger(name);
		}

		//
		// Summary:
		//     Returns the value of the given float parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		// Returns:
		//     The value of the parameter.
		inline float GetFloat(std::string name)
		{
			return controller->GetFloat(name);
		}

		inline unsigned int GetDuration()
		{
			if (sound != nullptr)
			{
				return sound->getPlayLength();
			}
			else
			{
				return 1;
			}
		}

		inline unsigned int GetCurrentProgression()
		{
			if (sound != nullptr)
			{
				return sound->getPlayPosition();
			}
			else
			{
				return 0;
			}
		}

		inline void SetCurrentProgression(unsigned int pos)
		{
			if (sound != nullptr)
			{
				sound->setPlayPosition(pos);
			}
		}

		inline bool isPlaying()
		{
			if (sound != nullptr)
			{
				return !sound->isFinished();
			}
			else
			{
				return false;
			}
		}

		//
		// Summary:
		//     Sets the value of the given boolean parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		//   value:
		//     The new parameter value.
		inline void SetBool(std::string name, bool value)
		{
			return controller->SetBool(name, value);
		}

		//
		// Summary:
		//     Sets the value of the given integer parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		//   value:
		//     The new parameter value.
		inline void SetInteger(std::string name, int value)
		{
			return controller->SetInteger(name, value);
		}

		//
		// Summary:
		//     Sets the value of the given float parameter.
		//
		// Parameters:
		//   name:
		//     The parameter name.
		//
		//   id:
		//     The parameter ID.
		//
		//   value:
		//     The new parameter value.
		inline void SetFloat(std::string name, float value)
		{
			return controller->SetFloat(name, value);
		}

		/*int GetComponentClassID() override { return 95; }
		std::string GetComponentClassData() override
		{
			std::string data = "";
			data += "Animator:\n";
			data += "  m_GameObject: {fileID: ";
			data += std::to_string(uniqueID);
			data += "}\n";
			data += "  m_Enable: ";
			data += this->enabled;
			data += "\n";
			return data;
		}*/

		/*bool Init() override
		{
			return true;
		}*/

		void Play();

		void Play(AudioSource* audio);

		void Pause();
		void Stop();

		void Update();
	};
}