#pragma once

#include <iostream>
#include "SDL.h"
#include "SDL_image.h"
#include "irrKlang.h"
#include <imnodes.h>

#include <map>
#include <string>

#include "AudioPlayer.h"

#include "Inspector.h"
#include "PlaygroundWindow.h"
#include "MainMenuBar.h"
#include "ParameterWindow.h"

#include "imgui.h"
#include "imgui_sdl.h"

constexpr int SCREEN_WIDTH = 1920;
constexpr int SCREEN_HEIGHT = 1080;
constexpr SDL_Color DARK = { 30, 30, 30, 255 };

namespace KHORA
{
	class Engine
	{
	private:
		SDL_Color m_ClearColor;
		SDL_Window* m_Window;
		static Engine* s_Instance;

		bool welcomePopup = true;

		std::map<std::string, SDL_Texture*> m_Textures;

		std::string m_togglePlayPath = "0169_play.png";
		bool m_PlayMode = false;

		// Windows settings
		bool m_SceneWindow = true;

		bool m_IsRunning;

		bool m_UnsaveChange = false;

		// AudioPlayer
		AudioPlayer audioPlayer;

		void LoadGUI();

		void ShowWelcomePopup();

		void SetupDockSpace();

		bool Init();
		void Clean();

		void Update();
		void Render();
		void Events();

	public:
		SDL_Renderer* m_Renderer;
		static SDL_Event s_Event;

		static Engine& GetInstance()
		{
			if (s_Instance == nullptr)
				s_Instance = new Engine();

			return *s_Instance;
		}

		void NewController();
		void OpenController(std::string name);

		inline void SaveChanges() 
		{
			PlaygroundWindow::GetInstance().SaveData();
		}
		void Refresh();
		inline void TriggerUnsaveChange() { m_UnsaveChange = true; }
		inline bool* GetUnsaveChange() { return &m_UnsaveChange; }

		AudioPlayer& GetAudioPlayer() { return audioPlayer; }

		SDL_Texture* GetTexture(std::string name) 
		{
			return m_Textures[name];
		}

		void Run();
		void Quit();
	};
}


