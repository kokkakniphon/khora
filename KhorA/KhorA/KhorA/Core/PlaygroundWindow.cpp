#include "PlaygroundWindow.h"

#include <stdio.h> 

#include "Input.h"
#include "Engine.h"

#include "example/save_load.cpp"

#define ASSET_EDITR_PATH "../Editor/"

namespace KHORA
{
	PlaygroundWindow* PlaygroundWindow::s_Instance = nullptr;

	void PlaygroundWindow::SaveData()
	{
		if (controller == nullptr)
			return;

		// Save the internal imnodes state
		imnodes::SaveCurrentEditorStateToIniFile((ASSET_EDITR_PATH + controller->name + ".ini").c_str());

		// Dump our editor state as bytes into a file

		std::fstream fout(
			(ASSET_EDITR_PATH + controller->name + ".bytes").c_str(), std::ios_base::out | std::ios_base::binary | std::ios_base::trunc);

		// copy the node vector to file
		const size_t num_nodes = m_Nodes.size();
		fout.write(
			reinterpret_cast<const char*>(&num_nodes),
			static_cast<std::streamsize>(sizeof(size_t)));
		fout.write(
			reinterpret_cast<const char*>(m_Nodes.data()),
			static_cast<std::streamsize>(sizeof(Node) * num_nodes));

		// copy the link vector to file
		const size_t num_links = m_Links.size();
		fout.write(
			reinterpret_cast<const char*>(&num_links),
			static_cast<std::streamsize>(sizeof(size_t)));
		fout.write(
			reinterpret_cast<const char*>(m_Links.data()),
			static_cast<std::streamsize>(sizeof(Link) * num_links));

		// copy the current_id to file
		fout.write(
			reinterpret_cast<const char*>(&m_CurrentID), static_cast<std::streamsize>(sizeof(int)));

		FileManager::WriteYAMLData(controller);
	}

	void PlaygroundWindow::LoadData(AudioController* controller)
	{
		m_Nodes.clear();

		this->controller = controller;

		currentControllerEditName = controller->name;

		// Load the internal imnodes state
		imnodes::LoadCurrentEditorStateFromIniFile((ASSET_EDITR_PATH + controller->name + ".ini").c_str());

		// Load our editor state into memory
		std::fstream fin((ASSET_EDITR_PATH + controller->name + ".bytes").c_str(), std::ios_base::in | std::ios_base::binary);

		if (!fin.is_open())
		{
			const int node_id = ++m_CurrentID;
			imnodes::SetNodeScreenSpacePos(node_id, ImVec2(200, 200));
			m_Nodes.push_back(Node(node_id, nullptr));

			m_Nodes[0].value = (controller->audioNodes[0].second).get();

			return;
		}

		// copy nodes into memory
		size_t num_nodes;
		fin.read(reinterpret_cast<char*>(&num_nodes), static_cast<std::streamsize>(sizeof(size_t)));
		m_Nodes.resize(num_nodes);
		fin.read(
			reinterpret_cast<char*>(m_Nodes.data()),
			static_cast<std::streamsize>(sizeof(Node) * num_nodes));

		// copy links into memory
		size_t num_links;
		fin.read(reinterpret_cast<char*>(&num_links), static_cast<std::streamsize>(sizeof(size_t)));
		m_Links.resize(num_links);
		fin.read(
			reinterpret_cast<char*>(m_Links.data()),
			static_cast<std::streamsize>(sizeof(Link) * num_links));

		// copy current_id into memory
		fin.read(reinterpret_cast<char*>(&m_CurrentID), static_cast<std::streamsize>(sizeof(int)));

		if (m_Nodes.size() <= 0)
		{
			const int node_id = ++m_CurrentID;
			imnodes::SetNodeScreenSpacePos(node_id, ImVec2(200, 200));
			m_Nodes.push_back(Node(node_id, nullptr));
		}

		// Setup Nodes
		for (unsigned int i = 0; i < controller->audioNodes.size(); i++)
		{
			m_Nodes[i].value = (controller->audioNodes[i].second).get();
		}

		for (Link& link : m_Links)
		{
			for (Node& node_s : m_Nodes)
			{
				for (Node& node_e : m_Nodes)
				{
					if (link.start_attr == (node_s.id << 24) && link.end_attr == (node_e.id << 8))
					{
						link.value = &node_s.value->destinations[node_e.value->name];
					}
				}
			}
		}
	}

	void PlaygroundWindow::FetchInput()
	{
		if (imnodes::IsEditorHovered())
		{
			if (ImGui::IsWindowFocused(ImGuiFocusedFlags_RootAndChildWindows) && controller != nullptr)
			{
				if (Input::GetKey(SDL_SCANCODE_LCTRL) && Input::GetKeyDown(SDL_SCANCODE_A))
				{
					const int node_id = ++m_CurrentID;
					imnodes::SetNodeScreenSpacePos(node_id, ImGui::GetMousePos());
					m_Nodes.push_back(Node(node_id, controller->CreateAudioNode("New Audio", 1.0f, false, "")));

					Engine::GetInstance().TriggerUnsaveChange();
				}

				if (Input::GetKey(SDL_SCANCODE_LCTRL) && Input::GetKeyDown(SDL_SCANCODE_D))
				{
					if (m_CurrentSelectedNode != -1)
					{
						if (m_Nodes[m_CurrentSelectedNode].value->name != "Entry")
						{
							for (size_t i = 0; i < m_Links.size();)
							{
								if (m_Links[i].start_attr == (m_Nodes[m_CurrentSelectedNode].id << 24) || m_Links[i].end_attr == (m_Nodes[m_CurrentSelectedNode].id << 8))
								{
									DeleteLink(i);
									i = 0;
								}
								else
								{
									i++;
								}
							}

							controller->RemoveAudioNode(m_Nodes[m_CurrentSelectedNode].value->name);
							m_Nodes.erase(m_Nodes.begin() + m_CurrentSelectedNode);
							m_CurrentSelectedNode = -1;

							Engine::GetInstance().TriggerUnsaveChange();
						}
					}

					if (m_CurrentSelectedLink != -1)
					{
						DeleteLink(m_CurrentSelectedLink);
						m_CurrentSelectedLink = -1;
						
						Engine::GetInstance().TriggerUnsaveChange();
					}
				}
			}

			if (Input::GetMouseButtonDown(SDL_BUTTON_MIDDLE))
			{
				if (m_Nodes.size() > 0)
				{
					m_InitNodesPos.resize(m_Nodes.size());
					for (unsigned int i = 0; i < m_Nodes.size(); i++)
					{
						m_InitNodesPos[i] = imnodes::GetNodeGridSpacePos(m_Nodes[i].id);
					}
				}

				m_MouseDownPos = Input::GetMousePosition();
			}

			if (Input::GetMouseButton(SDL_BUTTON_MIDDLE))
			{
				Vector2 m_CurrentGridPos = (Input::GetMousePosition() - m_MouseDownPos);

				for (unsigned int i = 0; i < m_Nodes.size(); i++)
				{
					imnodes::SetNodeGridSpacePos(m_Nodes[i].id, ImVec2(m_CurrentGridPos.x + m_InitNodesPos[i].x, m_CurrentGridPos.y + m_InitNodesPos[i].y));
				}
			}
		}
	}

	void PlaygroundWindow::DeleteLink(int index)
	{
		bool deleted = false;
		// This is bad coding, will refacter later.
		for (size_t i = 0; i < m_Nodes.size(); i++)
		{
			for (size_t j = 0; j < m_Nodes.size(); j++)
			{
				if (m_Links[index].start_attr == (m_Nodes[i].id << 24) && m_Links[index].end_attr == (m_Nodes[j].id << 8))
				{
					controller->RemoveTransition(m_Nodes[i].value->name, m_Nodes[j].value->name);
					m_Links.erase(m_Links.begin() + index);
					deleted = true;
					break;
				}
			}
			if (deleted)
				break;
		}
	}

	void PlaygroundWindow::ShowWindow()
	{
		if (ImGui::Begin("Playground"))
		{
			if (controller != nullptr)
			{				
				ImGui::SetNextItemWidth(400.0f);
				ImGui::InputTextWithHint("##nodeName", "Controller's Name", &currentControllerEditName);
				ImGui::SameLine();
				if (ImGui::ImageButton(Engine::GetInstance().GetTexture("0086_edit-2.png"), ImVec2(15.0f, 15.0f), ImVec2(0, 0), ImVec2(1, 1), -1, ImVec4(0, 0, 0, 0), ImVec4(1, 1, 1, 1)))
				{
					std::rename((ASSET_EDITR_PATH + controller->name + ".ini").c_str(), (ASSET_EDITR_PATH + currentControllerEditName + ".ini").c_str());
					std::rename((ASSET_EDITR_PATH + controller->name + ".bytes").c_str(), (ASSET_EDITR_PATH + currentControllerEditName + ".bytes").c_str());
					std::rename((ASSET_CONTROLLER_PATH + controller->name + ".SoundController").c_str(), (ASSET_CONTROLLER_PATH + currentControllerEditName + ".SoundController").c_str());
					controller->name = currentControllerEditName;

					Engine::GetInstance().Refresh();
				}
			}
			else
			{
				ImGui::TextColored(ImVec4(1.0f, 1.0f, 1.0f, 1.0f), "");
			}
			imnodes::BeginNodeEditor();
			{
				FetchInput();
			}

			std::string currentImg = (Engine::GetInstance().GetAudioPlayer().isPlaying()) ? "0203_square.png" : "0169_play.png";
			if (ImGui::ImageButton(Engine::GetInstance().GetTexture(currentImg), ImVec2(40.0f, 40.0f), ImVec2(0, 0), ImVec2(1, 1), -1, ImVec4(0, 0, 0, 0), ImVec4(1, 1, 1, 1)))
			{
				if (Engine::GetInstance().GetAudioPlayer().isPlaying())
				{
					Engine::GetInstance().GetAudioPlayer().Stop();
				}
				else
				{
					Engine::GetInstance().GetAudioPlayer().Play();
				}
			}

			for (Node& node : m_Nodes)
			{

				if (Engine::GetInstance().GetAudioPlayer().isPlaying() == true && node.value->name == Engine::GetInstance().GetAudioPlayer().GetCurrentAudioState())
				{
					imnodes::PushColorStyle(imnodes::ColorStyle_TitleBar, IM_COL32(255, 140, 0, 255));
					imnodes::PushColorStyle(
						imnodes::ColorStyle_TitleBarHovered, IM_COL32(255, 180, 0, 255));
					imnodes::PushColorStyle(
						imnodes::ColorStyle_TitleBarSelected, IM_COL32(255, 200, 0, 255));
				}
				
				imnodes::BeginNode(node.id);

				imnodes::BeginNodeTitleBar();
				ImGui::TextUnformatted(node.value->name.c_str());
				imnodes::EndNodeTitleBar();

				if (node.value->name != "Entry")
				{
					imnodes::BeginInputAttribute(node.id << 8);
					ImGui::TextUnformatted("input");
					imnodes::EndInputAttribute();
				}

				imnodes::BeginOutputAttribute(node.id << 24);
				const float text_width = ImGui::CalcTextSize("output").x;
				ImGui::Indent(text_width);
				ImGui::TextUnformatted("output");
				imnodes::EndOutputAttribute();

				imnodes::EndNode();

				if (Engine::GetInstance().GetAudioPlayer().isPlaying() == true && node.value->name == Engine::GetInstance().GetAudioPlayer().GetCurrentAudioState())
				{
					imnodes::PopColorStyle();
					imnodes::PopColorStyle();
					imnodes::PopColorStyle();
				}
			}

			for (const Link& link : m_Links)
			{
				imnodes::Link(link.id, link.start_attr, link.end_attr);
			}

			imnodes::EndNodeEditor();

			{
				Link link;
				if (imnodes::IsLinkCreated(&link.start_attr, &link.end_attr))
				{
					link.id = ++m_CurrentID;
					// This is bad coding, will refacter later.
					for (size_t i = 0; i < m_Nodes.size(); i++)
					{
						for (size_t j = 0; j < m_Nodes.size(); j++)
						{
							if (link.start_attr == (m_Nodes[i].id << 24) && link.end_attr == (m_Nodes[j].id << 8))
							{
								link.value = controller->AddTransition(m_Nodes[i].value->name, m_Nodes[j].value->name);
								m_Links.push_back(link);
							}
						}
					}
					
				}
			}

			{
				int link_id;
				if (imnodes::IsLinkDestroyed(&link_id))
				{
					auto iter =
						std::find_if(m_Links.begin(), m_Links.end(), [link_id](const Link& link) -> bool {
						return link.id == link_id;
					});
					assert(iter != m_Links.end());
					m_Links.erase(iter);
				}
			}

			

			{
				const int num_selected_nodes = imnodes::NumSelectedNodes();
				if (num_selected_nodes > 0)
				{
					std::vector<int> selected_nodes;
					selected_nodes.resize(num_selected_nodes);
					imnodes::GetSelectedNodes(selected_nodes.data());
					for (unsigned int i = 0; i < m_Nodes.size(); i++)
					{
						if (m_Nodes[i].id == selected_nodes[0])
						{
							m_CurrentSelectedNode = i;
							break;
						}
					}
				}
				else
				{
					m_CurrentSelectedNode = -1;
				}

				const int num_selected_links = imnodes::NumSelectedLinks();
				if (num_selected_links > 0)
				{
					std::vector<int> selected_links;
					selected_links.resize(num_selected_links);
					imnodes::GetSelectedLinks(selected_links.data());
					for (unsigned int i = 0; i < m_Links.size(); i++)
					{
						if (m_Links[i].id == selected_links[0])
						{
							m_CurrentSelectedLink = i;
							break;
						}
					}
				}
				else
				{
					m_CurrentSelectedLink = -1;
				}
			}

		}
		ImGui::End();
		
	}
}

