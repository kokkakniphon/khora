#include "ParameterWindow.h"

#include "Engine.h"

namespace KHORA
{
	ParameterWindow* ParameterWindow::s_Instance = nullptr;

	void ParameterWindow::FetchData()
	{
		globalParameter = Engine::GetInstance().GetAudioPlayer().GetController()->GetGlobalParameters();
	}

	void ParameterWindow::ShowWindow()
	{
		if (ImGui::Begin("Parameter"))
		{
			if (globalParameter != nullptr)
			{
				for (unsigned int i = 0; i < globalParameter->size(); i++)
				{
					ImGui::PushID(i);
					ImGui::InputTextWithHint("##parameter", "Parameter's name", &(*globalParameter)[i].name);

					if ((*globalParameter)[i].type == ParameterType::Boolean)
					{
						ImGui::SameLine();
						ImGui::Checkbox("##linkBool", &(*globalParameter)[i].currentValueBool);
						Engine::GetInstance().GetAudioPlayer().SetBool((*globalParameter)[i].name, (*globalParameter)[i].currentValueBool);
					}

					if ((*globalParameter)[i].type == ParameterType::Integer)
					{
						ImGui::SameLine();
						ImGui::InputInt("##linkInt", &(*globalParameter)[i].currentValueInt);
						Engine::GetInstance().GetAudioPlayer().SetInteger((*globalParameter)[i].name, (*globalParameter)[i].currentValueInt);
					}

					if ((*globalParameter)[i].type == ParameterType::Float)
					{
						ImGui::SameLine();
						ImGui::InputFloat("##linkInt", &(*globalParameter)[i].currentValueFloat);
						Engine::GetInstance().GetAudioPlayer().SetFloat((*globalParameter)[i].name, (*globalParameter)[i].currentValueFloat);
					}

					ImGui::PopID();
				}

				ImGui::Separator();

				ImGui::Spacing();
				ImGui::SameLine(0.0f, ImGui::GetWindowWidth() - 80.0f);

				if (ImGui::ImageButton(Engine::GetInstance().GetTexture("0170_plus-circle.png"), ImVec2(20.0f, 20.0f), ImVec2(0, 0), ImVec2(1, 1), -1, ImVec4(0, 0, 0, 0), ImVec4(1, 1, 1, 1)))
				{
					ImGui::OpenPopup("my_select_popup");					
				}

				if (ImGui::BeginPopup("my_select_popup"))
				{
					const char* names[] = { "Interger", "Float", "Boolean" };

					ImGui::Text("Parameter Type");
					ImGui::Separator();
					for (int i = 0; i < IM_ARRAYSIZE(names); i++)
					{
						if (ImGui::Selectable(names[i]))
						{
							ParameterType type = static_cast<ParameterType>(i);
							std::cout << "Parameter Type: " << type << std::endl;
							globalParameter->push_back(Parameter("New Parameter", type));
						}
					}
					ImGui::EndPopup();
				}

				ImGui::SameLine();

				if (ImGui::ImageButton(Engine::GetInstance().GetTexture("0143_minus-circle.png"), ImVec2(20.0f, 20.0f), ImVec2(0, 0), ImVec2(1, 1), -1, ImVec4(0, 0, 0, 0), ImVec4(1, 1, 1, 1)))
				{
					if (globalParameter->size() > 0)
						globalParameter->pop_back();
				}
			}
		}
		ImGui::End();
	}

}