#pragma once

#include "imgui.h"
#include "imnodes.h"

#include "Vector2D.h"
#include "AudioPlayer.h"

#include <algorithm>
#include <cassert>
#include <fstream>
#include <ios> // for std::streamsize
#include <stddef.h>
#include <vector>
#include <iostream>

namespace KHORA
{
	struct Node
	{
		int id;
		AudioSource* value;

		Node() = default;

		Node(const int i, AudioSource* v) : id(i), value(v) {}
	};

	struct Link
	{
		int id;
		std::vector<Parameter>* value;
		int start_attr, end_attr;
	};

	class PlaygroundWindow
	{
	private:
		AudioController* controller;

		std::vector<Node> m_Nodes;
		std::vector<Link> m_Links;
		int m_CurrentID;
		
		std::vector<ImVec2> m_InitNodesPos;
		std::vector<ImVec2> m_InitLinksPos;

		std::string currentControllerEditName;

		Vector2 m_MouseDownPos;

		int m_CurrentSelectedNode = -1;
		int m_CurrentSelectedLink = -1;

		static PlaygroundWindow* s_Instance;

		void FetchInput();

		void DeleteLink(int index);

	public:

		void ShowWindow();

		Node* GetCurrentSelectingNode() 
		{
			if (m_CurrentSelectedNode == -1)
				return nullptr;
			else
				return &m_Nodes[m_CurrentSelectedNode];
		}

		Link* GetCurrentSelectingLink()
		{
			if (m_CurrentSelectedLink == -1)
				return nullptr;
			else
				return &m_Links[m_CurrentSelectedLink];
		}

		inline static PlaygroundWindow& GetInstance()
		{
			if (s_Instance == nullptr)
			{
				s_Instance = new PlaygroundWindow();

				imnodes::GetIO().link_detach_with_modifier_click.modifier = &ImGui::GetIO().KeyCtrl;
				imnodes::PushAttributeFlag(imnodes::AttributeFlags_EnableLinkDetachWithDragClick);
			}
			return *s_Instance;
		}

		void SaveData();
		void LoadData(AudioController* controller);

		inline void Clean() 
		{ 
			imnodes::PopAttributeFlag();

			delete s_Instance; 
		}
	};
}
